output "database_instances" {
  value = "${google_compute_instance.mongo.*.self_link}"
}
