resource "google_compute_instance" "mongo" {
  count        = "${var.database_replica_count}"
  name         = "mongo-${count.index}"
  zone         = "${var.zone}"
  machine_type = "n1-highmem-2"

  boot_disk {
    initialize_params {
      image = "${var.ubuntu_image}"
    }
  }

  attached_disk {
    source      = "${element(google_compute_disk.mongo.*.self_link, count.index)}"
    device_name = "mongo-data"
  }

  network_interface {
    network = "${var.network_self_link}"
    access_config {}
  }

  tags = [
    "consul",
  ]

  metadata = {
    role = "database"
  }

  service_account {
    scopes = ["userinfo-email", "compute-ro", "storage-ro"]
  }

  provisioner "remote-exec" {
    scripts = [
      "${path.module}/ubuntu/install-basic-software.sh",
      "${path.module}/install-consul.sh",
      "${path.module}/install-mongo.sh",
    ]
  }

  connection {
    user        = "${var.ssh_user}"
    private_key = "${file(var.ssh_private_key)}"
  }

  provisioner "file" {
    content     = "${var.consul_retry_join}"
    destination = "/tmp/retry-join.json"
  }

  provisioner "file" {
    content     = "${var.consul_base}"
    destination = "/tmp/base.json"
  }

  provisioner "file" {
    source      = "${path.module}/mongo-check.json"
    destination = "/tmp/tcp-check.json"
  }

  provisioner "file" {
    content     = "${var.consul_systemd_unit}"
    destination = "/tmp/consul.service"
  }

  provisioner "file" {
    source      = "${path.module}/coreos-metadata.service"
    destination = "/tmp/coreos-metadata.service"
  }

  provisioner "file" {
    source      = "${path.module}/mongo.conf"
    destination = "/tmp/mongo.conf"
  }

  provisioner "remote-exec" {
    inline = [
      "sudo mv /tmp/*.json /etc/consul",
      "sudo mv /tmp/*.service /lib/systemd/system",
      "sudo mv /tmp/mongo.conf /etc/mongo.conf",
      "sudo systemctl daemon-reload",
      "sudo systemctl reenable consul",
      "sudo systemctl restart consul",
      "sudo systemctl reenable mongod",
      "sudo systemctl restart mongod",
      "sync"
    ]
  }
}

resource "google_compute_disk" "mongo" {
  count = "${var.database_replica_count}"
  name  = "mongo-data-${count.index}"
  type  = "pd-ssd"
  size  = "100"
  zone  = "${var.zone}"
}
