#!/bin/sh

set -e

sudo mkdir --parents /etc/consul
sudo useradd --create-home --home-dir /var/lib/consul --system --shell /bin/false consul
sudo curl --silent --output /usr/bin/consul https://storage.googleapis.com/adrienne-devops-public-infrastructure/consul
sudo chmod 0755 /usr/bin/consul
