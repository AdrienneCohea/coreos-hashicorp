output "vault_password" {
    value = "${random_string.vault_password.result}"
}

output "mysql_external_ip" {
    value = "${google_compute_instance.primary.network_interface.0.access_config.0.nat_ip}"
}

output "mysql_internal_ip" {
    value = "${google_compute_instance.primary.network_interface.0.network_ip}"
}
