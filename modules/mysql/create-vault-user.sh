#!/bin/sh

set -e

MYSQL_VAULT_PASSWORD=${1}

sudo mysql -e "DROP USER IF EXISTS 'vault'@'%'; CREATE USER 'vault'@'%' identified by '${MYSQL_VAULT_PASSWORD}'; GRANT ALL PRIVILEGES ON *.* to 'vault'@'%' WITH GRANT OPTION; FLUSH PRIVILEGES;"
sudo mysql -e "DROP USER IF EXISTS 'vault'@'localhost'; CREATE USER 'vault'@'localhost' identified by '${MYSQL_VAULT_PASSWORD}'; GRANT ALL PRIVILEGES ON *.* to 'vault'@'localhost' WITH GRANT OPTION; FLUSH PRIVILEGES;"
