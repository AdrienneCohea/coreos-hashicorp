resource "google_compute_instance" "primary" {
  name         = "mysql-primary"
  zone         = "${var.zone}"
  machine_type = "n1-highmem-2"

  boot_disk {
    initialize_params {
      image = "${var.ubuntu_image}"
    }
  }

  attached_disk {
    source      = "${google_compute_disk.primary.self_link}"
    device_name = "mysql-data"
  }

  network_interface {
    network = "${var.network_self_link}"
    access_config {}
  }

  tags = [
    "consul",
  ]

  metadata = {
    role = "database"
  }

  service_account {
    scopes = ["userinfo-email", "compute-ro", "storage-ro"]
  }

  provisioner "remote-exec" {
    scripts = [
      "${path.module}/install-consul.sh",
      "${path.module}/install-mysql.sh",
    ]
  }

  connection {
    user        = "${var.ssh_user}"
    private_key = "${file(var.ssh_private_key)}"
  }

  provisioner "file" {
    content     = "${var.consul_retry_join}"
    destination = "/tmp/retry-join.json"
  }

  provisioner "file" {
    content     = "${var.consul_base}"
    destination = "/tmp/base.json"
  }

  provisioner "file" {
    source      = "${path.module}/mysql-service.json"
    destination = "/tmp/mysql.json"
  }

  provisioner "file" {
    content     = "${var.consul_systemd_unit}"
    destination = "/tmp/consul.service"
  }

  provisioner "file" {
    source      = "${path.module}/coreos-metadata.service"
    destination = "/tmp/coreos-metadata.service"
  }

  provisioner "file" {
    source      = "${path.module}/mysqld.cnf"
    destination = "/tmp/mysqld.cnf"
  }

  # Network

  provisioner "file" {
    content = "${data.template_file.dummy0_netdev.rendered}"
    destination = "/tmp/dummy0.netdev"
  }

  provisioner "file" {
    content = "${data.template_file.dummy0_network.rendered}"
    destination = "/tmp/dummy0.network"
  }
  
  provisioner "remote-exec" {
    inline = [
      "sudo mv /tmp/dummy0.netdev /lib/systemd/network",
      "sudo mv /tmp/dummy0.network /lib/systemd/network",
      "sudo systemctl restart systemd-networkd",
    ]
  }

  provisioner "remote-exec" {
    inline = [
      "wget https://storage.googleapis.com/adrienne-devops-public-infrastructure/afterburn",
      "chmod +x afterburn",
      "sudo rm -f /etc/mysql/mysql.conf.d/mysqld.cnf",
      "sudo mv /tmp/mysqld.cnf /etc/mysql/mysql.conf.d/mysqld.cnf",
      "sudo mv afterburn /usr/bin/coreos-metadata",
      "sudo mv /tmp/*.json /etc/consul",
      "sudo mv /tmp/*.service /lib/systemd/system",
      "sudo systemctl daemon-reload",
      "sudo systemctl reenable consul",
      "sudo systemctl restart consul",
      "sudo systemctl reenable mysql",
      "sudo systemctl restart mysql",
      "sync"
    ]
  }
}

resource "google_compute_disk" "primary" {
  name  = "mysql-primary-data"
  type  = "pd-ssd"
  size  = "100"
  zone  = "${var.zone}"
}
