resource "null_resource" "create_vault_user" {
  triggers = {
    random_password = "${random_string.vault_password.result}"
    id              = "${google_compute_instance.primary.id}"
    script          = "${file("${path.module}/create-vault-user.sh")}"
  }

  provisioner "file" {
    connection {
        type        = "ssh"
        user        = "${var.ssh_user}"
        private_key = "${file(var.ssh_private_key)}"
        host        = "${google_compute_instance.primary.network_interface.0.access_config.0.nat_ip}"
    }

    content = "${file("${path.module}/create-vault-user.sh")}"
    destination = "/tmp/create-vault-user.sh"
  }

  provisioner "remote-exec" {
    connection {
        type        = "ssh"
        user        = "${var.ssh_user}"
        private_key = "${file(var.ssh_private_key)}"
        host        = "${google_compute_instance.primary.network_interface.0.access_config.0.nat_ip}"
    }

    inline = [
      "chmod +x /tmp/create-vault-user.sh",
      "sudo mv /tmp/create-vault-user.sh /usr/bin/create-vault-user.sh",
      "/usr/bin/create-vault-user.sh ${random_string.vault_password.result}"
    ]
  }
}
