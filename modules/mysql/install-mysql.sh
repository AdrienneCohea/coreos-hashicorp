#!/bin/sh

set -e

sudo apt update
sudo apt upgrade -y
sudo apt install -y mysql-server
