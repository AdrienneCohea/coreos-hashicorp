data "template_file" "dummy0_netdev" {
  template = "${file("${path.module}/dummy0.netdev")}"
}

data "template_file" "dummy0_network" {
  template = "${file("${path.module}/dummy0.network")}"
}
