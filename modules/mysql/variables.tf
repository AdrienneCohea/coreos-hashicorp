variable "region" {}
variable "zone" {}
variable "project" {}
variable "ssh_user" {}
variable "ssh_private_key" {}
variable "network_self_link" {}
variable "consul_retry_join" {}
variable "consul_base" {}
variable "consul_systemd_unit" {}
variable "ubuntu_image" {}
