resource "random_string" "vault_password" {
  length        = 24
  min_upper     = 6
  min_lower     = 4
  min_numeric   = 3
  special       = false
}
