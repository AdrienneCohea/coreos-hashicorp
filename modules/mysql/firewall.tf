resource "google_compute_firewall" "allow_mysql_internal" {
  name    = "allow-mysql-internal"
  network = "${var.network_self_link}"

  allow {
    protocol = "tcp"
    ports    = ["3306"]
  }

  source_tags = [
    "vault",
    "nomad",
    "compute-instance"
  ]
}
