resource "google_compute_instance_template" "compute_instance" {
  name_prefix = "compute-instance-"
  description = "A compute node powered by Nomad."

  tags = [
    "compute-instance",
    "consul",
    "nomad",
    "vault",
  ]

  instance_description = "A compute node powered by Nomad."
  machine_type         = "n1-highcpu-4"

  disk {
    source_image = "coreos-cloud/coreos-stable"
    boot         = true
  }

  network_interface {
    network = "${var.network_self_link}"
    access_config = {}
  }

  metadata = {
    "user-data" = "${data.ignition_config.compute_instance.rendered}"
  }

  service_account {
    scopes = ["userinfo-email", "compute-ro", "storage-ro"]
  }

  lifecycle {
    create_before_destroy = true
  }
}
