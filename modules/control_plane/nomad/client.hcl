client {
  enabled           = true
  network_interface = "ens4v1"

  options = {
    "driver.whitelist" = "docker"
    "driver.docker" = 1
    "driver.docker.version" = "18.06.3-ce"
  }
}
