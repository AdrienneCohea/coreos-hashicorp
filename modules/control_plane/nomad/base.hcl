
data_dir = "/var/lib/nomad"

advertise {
  http = "{{ GetInterfaceIP \"ens4v1\" }}"
  rpc  = "{{ GetInterfaceIP \"ens4v1\" }}"
  serf = "{{ GetInterfaceIP \"ens4v1\" }}"
}

datacenter = "${datacenter}"
