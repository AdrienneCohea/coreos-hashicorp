data "ignition_file" "nomad_base" {
    filesystem = "root"
    mode = 420
    path = "${data.ignition_directory.nomad_configuration.path}/base.hcl"
    content {
        content = "${data.template_file.nomad_base.rendered}"
    }
}

data "template_file" "nomad_base" {
  template   = "${file("${path.module}/nomad/base.hcl")}"

  vars = {
    datacenter = "${var.datacenter}"
  }
}

data "ignition_file" "nomad_autopilot" {
    filesystem = "root"
    mode = 420
    path = "${data.ignition_directory.nomad_configuration.path}/autopilot.hcl"
    content {
        content = "${data.template_file.nomad_autopilot.rendered}"
    }
}

data "template_file" "nomad_autopilot" {
  template = "${file("${path.module}/nomad/autopilot.hcl")}"
}

data "ignition_file" "nomad_server" {
    filesystem = "root"
    mode = 420
    path = "${data.ignition_directory.nomad_configuration.path}/server.hcl"
    content {
        content = "${data.template_file.nomad_server.rendered}"
    }
}

data "template_file" "nomad_server" {
  template = "${file("${path.module}/nomad/server.hcl")}"
}

data "ignition_file" "nomad_client" {
    filesystem = "root"
    mode = 420
    path = "${data.ignition_directory.nomad_configuration.path}/client.hcl"
    content {
        content = "${data.template_file.nomad_client.rendered}"
    }
}

data "template_file" "nomad_client" {
  template = "${file("${path.module}/nomad/client.hcl")}"
}
