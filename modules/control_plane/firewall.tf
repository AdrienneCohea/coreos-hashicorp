resource "google_compute_firewall" "allow_icmp" {
  name    = "allow-icmp"
  network = "${var.network_self_link}"

  allow {
    protocol = "icmp"
  }

  source_ranges = ["0.0.0.0/0"]
}

resource "google_compute_firewall" "allow_ssh" {
  name    = "allow-ssh"
  network = "${var.network_self_link}"

  allow {
    protocol = "tcp"
    ports    = ["22"]
  }

  source_ranges = ["0.0.0.0/0"]
}

resource "google_compute_firewall" "allow_consul_http" {
  name    = "allow-consul-http"
  network = "${var.network_self_link}"

  allow {
    protocol = "tcp"
    ports    = ["8500"]
  }

  source_ranges = ["0.0.0.0/0"]
}

resource "google_compute_firewall" "allow_consul_internal" {
  name    = "allow-consul-internal"
  network = "${var.network_self_link}"

  allow {
    protocol = "tcp"
    ports    = ["8300", "8301", "8302"]
  }

  allow {
    protocol = "udp"
    ports    = ["8300", "8301", "8302"]
  }

  source_tags = ["consul"]
}

resource "google_compute_firewall" "allow_nomad_internal" {
  name    = "allow-nomad-internal"
  network = "${var.network_self_link}"

  allow {
    protocol = "tcp"
    ports    = ["4647", "4648"]
  }

  allow {
    protocol = "udp"
    ports    = ["4647", "4648"]
  }

  source_tags = ["nomad"]
}

resource "google_compute_firewall" "allow_nomad_http" {
  name    = "allow-nomad-http"
  network = "${var.network_self_link}"

  allow {
    protocol = "tcp"
    ports    = ["4646"]
  }

  source_ranges = ["0.0.0.0/0"]
}

resource "google_compute_firewall" "allow_vault_internal" {
  name    = "allow-vault-internal"
  network = "${var.network_self_link}"

  allow {
    protocol = "tcp"
    ports    = ["8201", "8215"]
  }

  allow {
    protocol = "udp"
    ports    = ["8201", "8215"]
  }

  source_tags = ["vault"]
}

resource "google_compute_firewall" "allow_vault_http" {
  name    = "allow-vault-http"
  network = "${var.network_self_link}"

  allow {
    protocol = "tcp"
    ports    = ["8200"]
  }

  source_ranges = ["0.0.0.0/0"]
}

resource "google_compute_firewall" "allow_mongo_internal" {
  name    = "allow-mongo-internal"
  network = "${var.network_self_link}"

  allow {
    protocol = "tcp"
    ports    = ["27017"]
  }

  source_tags = [
    "vault",
    "nomad",
    "compute-instance"
  ]
}
