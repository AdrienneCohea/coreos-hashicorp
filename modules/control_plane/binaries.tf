data "ignition_file" "consul_binary" {
    filesystem = "root"
    mode = 493
    path = "${var.coreos_binary_path}/consul"
    source {
        source = "https://storage.googleapis.com/adrienne-devops-public-infrastructure/consul"
    }
}

data "ignition_file" "vault_binary" {
    filesystem = "root"
    mode = 493
    path = "${var.coreos_binary_path}/vault"
    source {
        source = "https://storage.googleapis.com/adrienne-devops-public-infrastructure/vault"
    }
}

data "ignition_file" "nomad_binary" {
    filesystem = "root"
    mode = 493
    path = "${var.coreos_binary_path}/nomad"
    source {
        source = "https://storage.googleapis.com/adrienne-devops-public-infrastructure/nomad"
    }
}
