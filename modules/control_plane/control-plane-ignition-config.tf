data "ignition_config" "control_plane" {
    users = [
        "${data.ignition_user.consul.id}",
        "${data.ignition_user.vault.id}",
        "${data.ignition_user.nomad_server.id}",
    ]

    directories = [
        "${data.ignition_directory.consul_configuration.id}",
        "${data.ignition_directory.vault_configuration.id}",
        "${data.ignition_directory.nomad_configuration.id}",
    ]
    
    files = [
        "${data.ignition_file.consul_binary.id}",
        "${data.ignition_file.vault_binary.id}",
        "${data.ignition_file.nomad_binary.id}",
        "${data.ignition_file.consul_base.id}",
        "${data.ignition_file.consul_server.id}",
        "${data.ignition_file.consul_retry_join.id}",
        "${data.ignition_file.vault_base.id}",
        "${data.ignition_file.vault_listener.id}",
        "${data.ignition_file.vault_storage.id}",
        "${data.ignition_file.nomad_base.id}",
        "${data.ignition_file.nomad_autopilot.id}",
        "${data.ignition_file.nomad_server.id}",
    ]
    
    systemd = [
        "${data.ignition_systemd_unit.consul.id}",
        "${data.ignition_systemd_unit.vault.id}",
        "${data.ignition_systemd_unit.nomad.id}",
    ]
    
    networkd = [
        "${data.ignition_networkd_unit.dummy0_netdev.id}",
        "${data.ignition_networkd_unit.dummy0_network.id}",
    ]
}
