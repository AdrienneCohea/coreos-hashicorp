data "ignition_networkd_unit" "dummy0_netdev" {
    name = "dummy0.netdev"
    content = "${data.template_file.dummy0_netdev.rendered}"
}

data "ignition_networkd_unit" "dummy0_network" {
    name = "dummy0.network"
    content = "${data.template_file.dummy0_network.rendered}"
}