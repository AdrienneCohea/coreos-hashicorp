output "control_plane_instances" {
  value = "${google_compute_instance.control_plane.*.self_link}"
}

output "compute_instances_instance_group" {
  value = "${google_compute_instance_group_manager.compute_instance_group.instance_group}"
}

output "consul_retry_join" {
  value = "${data.template_file.consul_retry_join.rendered}"
}

output "consul_base" {
  value = "${data.template_file.consul_base.rendered}"
}

output "consul_systemd_unit" {
  value = "${data.template_file.consul_service.rendered}"
}
