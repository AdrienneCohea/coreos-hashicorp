variable "region" {}
variable "zone" {}
variable "project" {}
variable "ssh_user" {}
variable "ssh_private_key" {}

# The name of the datacenter. This will be the same for Consul and Nomad.
variable "datacenter" {}

variable "network_self_link" {}

variable "ubuntu_image" {}

variable "coreos_binary_path" {
  default = "/opt/bin"
}

variable "binary_path" {
  default = "/usr/bin"
}
