data "ignition_systemd_unit" "nomad" {
    name = "nomad.service"
    content = "${data.template_file.nomad_service.rendered}"
}

data "ignition_systemd_unit" "vault" {
    name = "vault.service"
    content = "${data.template_file.vault_service.rendered}"
}

data "ignition_systemd_unit" "consul" {
    name = "consul.service"
    content = "${data.template_file.consul_service_coreos.rendered}"
}
