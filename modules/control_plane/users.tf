data "ignition_user" "consul" {
  name        = "consul"
  home_dir    = "/var/lib/consul"
  system      = true
}

data "ignition_user" "vault" {
  name        = "vault"
  system      = true
}

data "ignition_user" "nomad_client" {
  name        = "nomad"
  groups      = ["docker"]
  home_dir    = "/var/lib/nomad"
  system      = true
}

data "ignition_user" "nomad_server" {
  name        = "nomad"
  home_dir    = "/var/lib/nomad"
  system      = true
}
