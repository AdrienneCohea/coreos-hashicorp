#!/bin/sh

set -e

sudo apt update
sudo apt upgrade -yq
sudo apt install -yq jq unzip

curl --silent --output /tmp/afterburn https://storage.googleapis.com/adrienne-devops-public-infrastructure/afterburn
sudo mv /tmp/afterburn /usr/bin/coreos-metadata
sudo chmod 0755 /usr/bin/coreos-metadata
sudo chown root:root /usr/bin/coreos-metadata
sync
