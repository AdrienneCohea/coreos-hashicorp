data "ignition_file" "consul_base" {
    filesystem = "root"
    mode = 420
    path = "${data.ignition_directory.consul_configuration.path}/base.json"
    content {
        content = "${data.template_file.consul_base.rendered}"
    }
}

data "template_file" "consul_base" {
  template = "${file("${path.module}/consul/base.json")}"

  vars = {
      datacenter  = "${var.datacenter}"
  }
}

data "ignition_file" "consul_server" {
    filesystem = "root"
    mode = 420
    path = "${data.ignition_directory.consul_configuration.path}/server.json"
    content {
        content = "${data.template_file.consul_server.rendered}"
    }
}

data "template_file" "consul_server" {
  template = "${file("${path.module}/consul/server.json")}"
}

data "ignition_file" "consul_retry_join" {
    filesystem = "root"
    mode = 420
    path = "${data.ignition_directory.consul_configuration.path}/retry-join.json"
    content {
        content = "${data.template_file.consul_retry_join.rendered}"
    }
}

data "template_file" "consul_retry_join" {
  template = "${file("${path.module}/consul/gce/retry-join.json")}"

  vars = {
    project = "${var.project}"
  }
}
