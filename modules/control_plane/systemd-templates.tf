data "template_file" "consul_service_coreos" {
  template = "${file("${path.module}/consul/gce/consul.service")}"

    vars = {
    user        = "${data.ignition_user.consul.name}"
    group       = "${data.ignition_user.consul.name}"
    config_dir  = "${data.ignition_directory.consul_configuration.path}"
    binary_path = "${var.coreos_binary_path}"
  }
}

data "template_file" "consul_service" {
  template = "${file("${path.module}/consul/gce/consul.service")}"

    vars = {
    user        = "${data.ignition_user.consul.name}"
    group       = "${data.ignition_user.consul.name}"
    config_dir  = "${data.ignition_directory.consul_configuration.path}"
    binary_path = "${var.binary_path}"
  }
}

data "template_file" "nomad_service" {
  template = "${file("${path.module}/nomad.service")}"

  vars = {
    user        = "${data.ignition_user.nomad_server.name}"
    group       = "${data.ignition_user.nomad_server.name}"
    config_dir  = "${data.ignition_directory.nomad_configuration.path}"
    binary_path = "${var.coreos_binary_path}"
  }
}

data "template_file" "vault_service" {
  template = "${file("${path.module}/vault.service")}"

  vars = {
    user        = "${data.ignition_user.vault.name}"
    group       = "${data.ignition_user.vault.name}"
    config_dir  = "${data.ignition_directory.vault_configuration.path}"
    binary_path = "${var.binary_path}"
  }
}


