provider "google-beta" {
  credentials = "${file("~/.gcloud/account.json")}"
  project     = "${var.project}"
  region      = "${var.region}"
}
