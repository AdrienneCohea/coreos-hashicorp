resource "google_compute_instance" "control_plane" {
  count         = 3
  name          = "control-plane-${count.index + 1}"
  machine_type  = "n1-standard-1"
  zone          = "${var.zone}"
  description   = "A control plane node, with Consul, Vault and Nomad running in server mode."

  tags = [
    "control-plane",
    "consul",
    "nomad",
    "vault",
  ]

  boot_disk {
    initialize_params {
      image = "${var.ubuntu_image}"
    }
  }

  network_interface {
    network = "${var.network_self_link}"
    access_config = {}
  }

  service_account {
    scopes = ["userinfo-email", "compute-ro", "storage-ro"]
  }

  connection {
    user        = "${var.ssh_user}"
    private_key = "${file(var.ssh_private_key)}"
  }

  # Install software, create users and directories

  provisioner "remote-exec" {
    inline = [
      "sudo useradd --shell /bin/false --system --create-home --home-dir /var/lib/consul consul",
      "sudo useradd --shell /bin/false --system --no-create-home vault",
      "sudo useradd --shell /bin/false --system --create-home --home-dir /var/lib/nomad nomad",
      "sudo mkdir -p /etc/consul",
      "sudo mkdir -p /etc/vault",
      "sudo mkdir -p /etc/nomad",
      "wget https://storage.googleapis.com/adrienne-devops-public-infrastructure/consul",
      "wget https://storage.googleapis.com/adrienne-devops-public-infrastructure/vault",
      "wget https://storage.googleapis.com/adrienne-devops-public-infrastructure/nomad",
      "wget https://storage.googleapis.com/adrienne-devops-public-infrastructure/afterburn",
      "chmod +x consul",
      "chmod +x vault",
      "chmod +x nomad",
      "chmod +x afterburn",
      "sudo mv consul /usr/bin",
      "sudo mv vault /usr/bin",
      "sudo mv nomad /usr/bin",
      "sudo mv afterburn /usr/bin/coreos-metadata",
    ]
  }

  # Unit files for systemd

  provisioner "file" {
    content     = "${data.template_file.consul_service.rendered}"
    destination = "/tmp/consul.service"
  }

  provisioner "file" {
    content     = "${data.template_file.vault_service.rendered}"
    destination = "/tmp/vault.service"
  }

  provisioner "file" {
    content     = "${data.template_file.nomad_service.rendered}"
    destination = "/tmp/nomad.service"
  }

  provisioner "file" {
    content     = "${file("${path.module}/coreos-metadata.service")}"
    destination = "/tmp/coreos-metadata.service"
  }

  provisioner "remote-exec" {
    inline = [
      "sudo mv /tmp/consul.service /lib/systemd/system",
      "sudo mv /tmp/vault.service /lib/systemd/system",
      "sudo mv /tmp/nomad.service /lib/systemd/system",
      "sudo mv /tmp/coreos-metadata.service /lib/systemd/system",
      "sudo systemctl daemon-reload",
    ]
  }
  
  # Consul configuration files

  provisioner "file" {
    content     = "${data.template_file.consul_base.rendered}"
    destination = "/tmp/consul-base.json"
  }

  provisioner "file" {
    content     = "${data.template_file.consul_server.rendered}"
    destination = "/tmp/server.json"
  }

  provisioner "file" {
    content     = "${data.template_file.consul_retry_join.rendered}"
    destination = "/tmp/retry-join.json"
  }

  provisioner "remote-exec" {
    inline = [
      "sudo mv /tmp/consul-base.json /etc/consul/base.json",
      "sudo mv /tmp/server.json /etc/consul",
      "sudo mv /tmp/retry-join.json /etc/consul",
    ]
  }

  # Vault configuration files

  provisioner "file" {
    content     = "${data.template_file.vault_base.rendered}"
    destination = "/tmp/vault-base.hcl"
  }

  provisioner "file" {
    content     = "${data.template_file.vault_listener.rendered}"
    destination = "/tmp/listener.hcl"
  }

  provisioner "file" {
    content     = "${data.template_file.vault_storage.rendered}"
    destination = "/tmp/storage.hcl"
  }

  provisioner "remote-exec" {
    inline = [
      "sudo mv /tmp/vault-base.hcl /etc/vault/base.hcl",
      "sudo mv /tmp/listener.hcl /etc/vault",
      "sudo mv /tmp/storage.hcl /etc/vault",
    ]
  }

  # Nomad configuration files

  provisioner "file" {
    content     = "${data.template_file.nomad_base.rendered}"
    destination = "/tmp/nomad-base.hcl"
  }

  provisioner "file" {
    content     = "${data.template_file.nomad_autopilot.rendered}"
    destination = "/tmp/autopilot.hcl"
  }

  provisioner "file" {
    content     = "${data.template_file.nomad_server.rendered}"
    destination = "/tmp/server.hcl"
  }

  provisioner "remote-exec" {
    inline = [
      "sudo mv /tmp/nomad-base.hcl /etc/nomad/base.hcl",
      "sudo mv /tmp/autopilot.hcl /etc/nomad",
      "sudo mv /tmp/server.hcl /etc/nomad",
    ]
  }

  # Network

  provisioner "file" {
    content = "${data.template_file.dummy0_netdev.rendered}"
    destination = "/tmp/dummy0.netdev"
  }

  provisioner "file" {
    content = "${data.template_file.dummy0_network.rendered}"
    destination = "/tmp/dummy0.network"
  }
  
  provisioner "remote-exec" {
    inline = [
      "sudo mv /tmp/dummy0.netdev /lib/systemd/network",
      "sudo mv /tmp/dummy0.network /lib/systemd/network",
      "sudo systemctl restart systemd-networkd",
    ]
  }

  # Start services

  provisioner "remote-exec" {
    inline = [
      "sudo systemctl reenable consul",
      "sudo systemctl reenable vault",
      "sudo systemctl restart consul",
      "sudo systemctl restart vault",
    ]
  }
}
