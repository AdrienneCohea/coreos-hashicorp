data "ignition_directory" "consul_configuration" {
    filesystem = "root"
    mode = 493
    path = "/etc/consul"
}

data "ignition_directory" "vault_configuration" {
    filesystem = "root"
    mode = 493
    path = "/etc/vault"
}

data "ignition_directory" "nomad_configuration" {
    filesystem = "root"
    mode = 493
    path = "/etc/nomad"
}
