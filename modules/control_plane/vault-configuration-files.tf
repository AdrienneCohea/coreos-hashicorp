data "ignition_file" "vault_base" {
    filesystem = "root"
    mode = 420
    path = "${data.ignition_directory.vault_configuration.path}/base.hcl"
    content {
        content = "${data.template_file.vault_base.rendered}"
    }
}

data "template_file" "vault_base" {
  template = "${file("${path.module}/vault/base.hcl")}"
}

data "ignition_file" "vault_listener" {
    filesystem = "root"
    mode = 420
    path = "${data.ignition_directory.vault_configuration.path}/listener.hcl"
    content {
        content = "${data.template_file.vault_listener.rendered}"
    }
}

data "template_file" "vault_listener" {
  template = "${file("${path.module}/vault/listener.hcl")}"
}

data "ignition_file" "vault_storage" {
    filesystem = "root"
    mode = 420
    path = "${data.ignition_directory.vault_configuration.path}/storage.hcl"
    content {
        content = "${data.template_file.vault_storage.rendered}"
    }
}

data "template_file" "vault_storage" {
  template = "${file("${path.module}/vault/storage.hcl")}"
}
