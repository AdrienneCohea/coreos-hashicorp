resource "google_compute_instance_group_manager" "compute_instance_group" {
  name               = "compute-instances"
  base_instance_name = "compute-instance"
  zone               = "${var.zone}"
  target_size        = "2"
  provider           = "google-beta"

  wait_for_instances = true

  update_policy {
    type = "PROACTIVE"
    minimal_action = "REPLACE"
    max_surge_fixed = 1
    max_unavailable_fixed = 1
    min_ready_sec = 5
  }

  version {
    name = "blue"
    instance_template  = "${google_compute_instance_template.compute_instance.self_link}"
  }
}
