#!/bin/bash

# Before taking this node out of service, we would like to drain any
# workloads it is running on a best effort basis, and then stop 
# Nomad, such that it gracefully leaves the cluster. Similarly, we 
# would like Consul to gracefully leave.

if [ $(systemctl is-active nomad) == "active" ]; then

    # If Nomad is running but not healthy, don't try to drain the
    # workloads, since we're destroying this node anyway.

    curl -s --fail http://localhost:4646/v1/agent/health || exit 0

    nomad node drain -self -enable -deadline 5m || true

    # Stop Nomad.

    systemctl stop nomad || true
fi

if [ $(systemctl is-active consul) == "active" ]; then
    systemctl stop consul || true
fi
