#!/bin/sh

vault login $(jq .root_token vault_initial_secrets.json | tr --delete '"' | base64 --decode | gpg --decrypt)
