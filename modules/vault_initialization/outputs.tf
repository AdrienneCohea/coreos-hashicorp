output "internal_ip" {
    value = "${data.google_compute_instance.control_plane_instances.*.network_interface.0.network_ip}"
}

output "external_ip" {
    value = "${data.google_compute_instance.control_plane_instances.*.network_interface.0.access_config.0.nat_ip}"
}

output "initialization_id" {
    value = "${null_resource.initialize.id}"
}

output "unseal_id" {
    value = "${null_resource.unseal.id}"
}

output "login_id" {
    value = "${null_resource.login.id}"
}
