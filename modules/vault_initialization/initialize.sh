#!/bin/sh

vault operator init -status
STATUSCODE=$?

if test $STATUSCODE -eq 2; then
    echo "Initializing Vault."
    vault operator init \
        -format=json \
        -key-shares=5 \
        -key-threshold=3 \
        -pgp-keys="keybase:adriennecohea,keybase:adriennecohea,keybase:adriennecohea,keybase:adriennecohea,keybase:adriennecohea" \
        -root-token-pgp-key="keybase:adriennecohea" > vault_initial_secrets.json
else
    echo "Vault already initialized."
fi
