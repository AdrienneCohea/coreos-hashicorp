resource "null_resource" "initialize" {
  triggers = {
    cluster_instance_ids = "${join(" ", data.google_compute_instance.control_plane_instances.*.network_interface.0.access_config.0.nat_ip)}"
    script = "${file("${path.module}/initialize.sh")}"
  }

  provisioner "local-exec" {
    command = "${path.module}/initialize.sh"

    environment = {
      VAULT_ADDR = "http://${element(data.google_compute_instance.control_plane_instances.*.network_interface.0.access_config.0.nat_ip, 0)}:8200"
    }
  }
}
