#!/bin/sh

set -e

if [ "$(vault secrets list -format=json | sed 's/\/":/":/g' | jq .database)" == "null" ]; then
    vault secrets enable database
fi
