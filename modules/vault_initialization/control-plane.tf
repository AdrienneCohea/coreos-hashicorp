data "google_compute_instance" "control_plane_instances" {
    count     = "${length(var.control_plane_instances)}"
    self_link = "${element(var.control_plane_instances, count.index)}"
}
