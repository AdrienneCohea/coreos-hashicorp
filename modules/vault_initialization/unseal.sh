#!/bin/sh

VAULT_IP_ADDRESSES="$@"

for VAULT_IP_ADDRESS in $VAULT_IP_ADDRESSES; do
    vault status -address=http://${VAULT_IP_ADDRESS}:8200 2>&1 > /dev/null
    STATUSCODE=$?

    if test $STATUSCODE -eq 2; then
        echo "Unsealing Vault."
        KEY1=$(jq .unseal_keys_b64[0] vault_initial_secrets.json | tr --delete '"' | base64 --decode | gpg --decrypt)
        vault operator unseal -address=http://${VAULT_IP_ADDRESS}:8200 $KEY1

        KEY2=$(jq .unseal_keys_b64[1] vault_initial_secrets.json | tr --delete '"' | base64 --decode | gpg --decrypt)
        vault operator unseal -address=http://${VAULT_IP_ADDRESS}:8200 $KEY2

        KEY3=$(jq .unseal_keys_b64[2] vault_initial_secrets.json | tr --delete '"' | base64 --decode | gpg --decrypt)
        vault operator unseal -address=http://${VAULT_IP_ADDRESS}:8200 $KEY3
    fi
done
