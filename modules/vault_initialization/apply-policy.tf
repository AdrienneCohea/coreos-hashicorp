resource "null_resource" "apply_policy" {
  triggers = {
    id     = "${null_resource.login.id}"
    script = "${file("${path.module}/apply-policy.sh")}"
  }

  provisioner "local-exec" {
    command = "${path.module}/apply-policy.sh"

    environment = {
      VAULT_ADDR = "http://${element(data.google_compute_instance.control_plane_instances.*.network_interface.0.access_config.0.nat_ip, 0)}:8200"
    }
  }
}
