resource "null_resource" "login" {
  triggers = {
    id = "${null_resource.unseal.id}"
    script = "${file("${path.module}/login.sh")}"
  }

  provisioner "local-exec" {
    command = "${path.module}/login.sh"

    environment = {
      VAULT_ADDR = "http://${element(data.google_compute_instance.control_plane_instances.*.network_interface.0.access_config.0.nat_ip, 0)}:8200"
    }
  }
}
