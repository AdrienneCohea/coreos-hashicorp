resource "null_resource" "unseal" {
  triggers = {
    id = "${null_resource.initialize.id}"
    script = "${file("${path.module}/unseal.sh")}"
  }

  provisioner "local-exec" {
    command = "${path.module}/unseal.sh ${join(" ", data.google_compute_instance.control_plane_instances.*.network_interface.0.access_config.0.nat_ip)}"
  }
}
