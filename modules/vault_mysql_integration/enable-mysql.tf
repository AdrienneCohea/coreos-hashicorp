resource "null_resource" "enable_mysql" {
  triggers = {
    initialization_id = "${var.initialization_id}"
    unseal_id         = "${var.unseal_id}"
    script            = "${file("${path.module}/enable-mysql.sh")}"
    mysql_ip          = "${var.mysql_ip}"
    vault_ip          = "${var.vault_ip}"
    vault_password    = "${var.vault_password}"
  }

  provisioner "local-exec" {
    command = "${path.module}/enable-mysql.sh"

    environment = {
      VAULT_ADDR           = "http://${var.vault_ip}:8200"
      MYSQL_INTERNAL_IP    = "${var.mysql_ip}"
      MYSQL_VAULT_PASSWORD = "${var.vault_password}"
    }
  }
}
