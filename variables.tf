variable "region" {}
variable "zone" {}
variable "project" {}
variable "ssh_user" {}
variable "ssh_private_key" {}

variable "ubuntu_image" {
  default = "ubuntu-os-cloud/ubuntu-1904"
}
