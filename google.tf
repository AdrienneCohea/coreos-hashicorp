provider "google" {
  credentials = "${file("~/.gcloud/account.json")}"
  project     = "${var.project}"
  region      = "${var.region}"
}
