module "control_plane" {
  source = "./modules/control_plane"

  region              = "${var.region}"
  zone                = "${var.zone}"
  project             = "${var.project}"
  ssh_user            = "${var.ssh_user}"
  ssh_private_key     = "${var.ssh_private_key}"
  network_self_link   = "${google_compute_network.staging.self_link}"
  datacenter          = "gce-staging"
  ubuntu_image        = "${var.ubuntu_image}"
}

module "vault_initialization" {
  source = "./modules/vault_initialization"

  control_plane_instances = "${module.control_plane.control_plane_instances}"
}

module "mysql" {
  source = "./modules/mysql"

  region              = "${var.region}"
  zone                = "${var.zone}"
  project             = "${var.project}"
  ssh_user            = "${var.ssh_user}"
  ssh_private_key     = "${var.ssh_private_key}"
  network_self_link   = "${google_compute_network.staging.self_link}"
  ubuntu_image        = "${var.ubuntu_image}"
  consul_retry_join   = "${module.control_plane.consul_retry_join}"
  consul_base         = "${module.control_plane.consul_base}"
  consul_systemd_unit = "${module.control_plane.consul_systemd_unit}"
}

module "vault_mysql_integration" {
  source = "./modules/vault_mysql_integration"

  initialization_id = "${module.vault_initialization.initialization_id}"
  unseal_id         = "${module.vault_initialization.unseal_id}"
  vault_password    = "${module.mysql.vault_password}"
  vault_ip          = "${element(module.vault_initialization.external_ip, 0)}"
  mysql_ip          = "${module.mysql.mysql_internal_ip}"
}
