terraform {
  backend "gcs" {
    bucket      = "adrienne-devops-infra-datacentersec"
    prefix      = "terraform/state"
    credentials = "~/.gcloud/account.json"
  }
}
